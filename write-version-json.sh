#!/bin/sh -xeu

cd -- "$(dirname -- "$0")"

commit="$(git rev-parse --abbrev-ref HEAD)@{#$(git rev-list --count --first-parent HEAD)}-"$(git log -1 --format=%cs)"-g$(git describe --always --match "" --dirty --abbrev=64)"

cat > version.json << EOF
{"commit": "$commit"}
EOF
